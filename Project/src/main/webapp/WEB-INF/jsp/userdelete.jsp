<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザ削除</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <button type="button" class="btn btn-link col d-flex justify-content-end ">ログアウト</button>
        <h1>ユーザ削除確認</h1>

        <h4>ログインID:〇〇〇を本当に削除してもよろしいでしょうか？</h4>


        <div class="form-group row">
            <div class="col-10">
                <button type="submit" class="btn btn-primary">キャンセル</button>
                <button type="submit" class="btn btn-primary">OK</button>
            </div>
        </div>




</body>

</html>