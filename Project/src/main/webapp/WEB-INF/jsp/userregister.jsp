<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザ新規登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body><h1>ユーザ新規登録</h1>
    <div class="container">
       
        <a href="LoginServlet" type="button" class="btn btn-link col d-flex justify-content-end ">ログアウト</a>


        <form action="UsersignupServlet" method="post">
            <div class="form-group row">
                <label for="inputEmail3" class="col-2 col-form-label">ログインID</label>
                <div class="col-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder=""name="loginId">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-2 col-form-label">パスワード</label>
                <div class="col-10">
                    <input type="password" class="form-control" id="inputPassword3" placeholder=""name="password">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-2 col-form-label">パスワード(確認)</label>
                <div class="col-10">
                    <input type="password" class="form-control" id="inputPassword3" placeholder=""name="password">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-2 col-form-label">ユーザ名</label>
                <div class="col-10">
                    <input type="text" class="form-control" id="inputPassword3" placeholder=""name="name">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-2 col-form-label">生年月日</label>
                <div class="col-10">
                    <input type="date" class="form-control" id="inputPassword3" placeholder=""name="birthdate">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-10">
                    <button type="submit" class="btn btn-primary">登録</button>
                </div>
            </div>
        </form>

        <a href="UserListServlet"type="button" class="btn btn-link col d-flex justify-content-start ">戻る</a>



    </div>


</body>

</html>