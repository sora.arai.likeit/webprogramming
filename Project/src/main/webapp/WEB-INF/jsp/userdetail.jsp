<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザ情報詳細参照</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <div class ="container">
    <div class="row">
        <div class="col d-flex justify-content-end">
             <a href="LoginServlet" type="button" class="btn btn-link col d-flex justify-content-end ">ログアウト</a>
        </div>
    </div>
    <h1>ユーザ情報詳細参照</h1>
    <p>ログインID</p>${user.loginId}
    <p>ユーザ名</p>${user.name}
    <p>生年月日</p>${user.birthDate}
    <p>登録日時</p>${user.createDate}
    <p>更新日時</p>${user.updateDate}


    <a href="UserListServlet"  class="btn btn-link">戻る</a>

</div>
</body>

</html>