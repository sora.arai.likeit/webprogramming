<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザ情報更新</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
    <div class ="container">

<a href="Loginservlet" type="button"
			class="btn btn-link col d-flex justify-content-end ">ログアウト</a>
			
        <h1>ユーザ情報更新</h1>

    
        <form action="UserUpdateServlet" method="post">
            <div class="form-group row">
                <label for="inputEmail3" class="col-2 col-form-label">ログインID</label>
                <div class="col-10">
                    <input type="text" class="form-control" id="inputEmail3" value="${user.loginId}" name="loginId">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-2 col-form-label">パスワード</label>
                <div class="col-10">
                    <input type="password" class="form-control" id="inputPassword3" placeholder="" name="password">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-2 col-form-label">パスワード(確認)</label>
                <div class="col-10">
                    <input type="password" class="form-control" id="inputPassword3" placeholder="" name="re_password">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-2 col-form-label">ユーザ名</label>
                <div class="col-10">
                    <input type="text" class="form-control" id="inputPassword3" value="${user.name}" name="name">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-2 col-form-label">生年月日</label>
                <div class="col-10">
                    <input type="date" class="form-control" id="inputPassword3" value="${user.birthDate}" name="birthDate">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-10">
                    <button type="submit" class="btn btn-primary">更新</button>
                </div>
            </div>
        </form>

					<a href="UserListServlet"  class="btn btn-link">戻る</a>

   
   






</body>

</html>