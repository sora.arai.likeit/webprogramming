<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ログイン画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <div class="container">
    
    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

        <h1>ログイン画面</h1>

         <form action="LoginServlet" method="post">
            <div class="form-group row">
                <label for="inputPassword2" class="col-sm-2 col-form-label">ログインID</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputPassword2" placeholder=""name="loginId">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">パスワード</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="inputPassword3" placeholder=""name="password">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-10">
                    <button type="submit" class="btn btn-primary">ログイン</button>
                </div>
            </div>
           
            
        </form>



    </div>

</body>

</html>