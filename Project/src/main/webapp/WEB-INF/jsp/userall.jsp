<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
	<div class="container">


		<a href="Loginservlet" type="button"
			class="btn btn-link col d-flex justify-content-end ">ログアウト</a>
		<h1>ユーザ一覧</h1>

		<a href="UsersignupServlet"
			class="btn btn-link col d-flex justify-content-end ">新規登録</a>

		<form>
			<div class="form-group row">
				<label for="inputEmail3" class="col-2 col-form-label">ログインID</label>
				<div class="col-10">
					<input type="email" class="form-control" id="inputEmail3"
						placeholder="">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputPassword3" class="col-2 col-form-label">ユーザ名</label>
				<div class="col-10">
					<input type="password" class="form-control" id="inputPassword3"
						placeholder="">
				</div>
			</div>
			<div class="form-group row">
				<div class="col-10">
					<button type="submit" class="btn btn-primary">検索</button>
				</div>
			</div>
		</form>

		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ログインID</th>
						<th>ユーザ名</th>
						<th>生年月日</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="user" items="${userList}">
						<tr>
							<td>${user.loginId}</td>
							<td>${user.name}</td>
							<td>${user.birthDate}</td>
							<!-- TODO 未実装；ログインボタンの表示制御を行う -->
							<td><a class="btn btn-primary"href="UserdetailServlet?id=${user.id}">詳細</a>
							    <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
							    <a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>


</body>

</html>