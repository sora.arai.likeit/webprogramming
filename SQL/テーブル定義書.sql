﻿CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;


use usermanagement;

CREATE TABLE user(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE  NOT NULL,
name varchar(255) UNIQUE NOT NULL,
birth_date DATE NOT NULL,
password varchar(255)NOT NULL,
create_date DATETIME NOT NULL,
update_date  DATETIME NOT NULL);

drop table user;

use usermanagement;
INSERT INTO user(id,login_id,name,birth_date,password,create_date,update_date)
VALUES(1,'admin','管理者',' 1996-1-5 ','0105',NOW(),NOW());
